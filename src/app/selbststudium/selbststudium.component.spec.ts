import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelbststudiumComponent } from './selbststudium.component';

describe('SelbststudiumComponent', () => {
  let component: SelbststudiumComponent;
  let fixture: ComponentFixture<SelbststudiumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelbststudiumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelbststudiumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
