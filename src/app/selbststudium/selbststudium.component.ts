import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-selbststudium',
  templateUrl: './selbststudium.component.html',
  styleUrls: ['./selbststudium.component.css']
})
export class SelbststudiumComponent implements OnInit {


  KURSE = [
    {
      name: "Web Developer Bootcamp",
      url: "https://www.udemy.com/the-web-developer-bootcamp/learn/v4/overview",
      leitung: "Colt Steele"
    },
    {
      name: "The Advanced Web Developer Bootcamp",
      url: "https://www.udemy.com/the-advanced-web-developer-bootcamp/learn/v4/content",
      leitung: "Colt Steele"
    },
    {
      name: "The Modern JavaScript Bootcamp ",
      url: "https://www.udemy.com/modern-javascript/learn/v4/overview",
      leitung: "Andrew Mead"
    }
  ]
  constructor() { }

  ngOnInit() {
  }

}
