import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-projekte',
  templateUrl: './projekte.component.html',
  styleUrls: ['./projekte.component.css']
})
export class ProjekteComponent implements OnInit {

  PROJEKTE = [
    {  name: "projekt #1",
        subName: "meine Portfolio-Webseite",
        tags: [
            "HTML",
            "CSS",
            "TS",
            "Angular5",
            "Bootstrap"
        ],
        text: "Mein Internetauftritt ist noch in Arbeit !!",
        link: "pauldieterich.de",
        git :"https://bitbucket.org/PaulDieterich/portfoliov4/src/master/"
    },
    {  name: "projekt #2",
        subName: "Rechtsanwältin Dieterich",
        tags: [
                "Worpress"
              ],
        text: "Aktuell noch in konzeption und planung",
        link:"rechtsanwaeltin-dieterich.de"
    }
    
  
  ]

  
  constructor() {
   
    }
   
  ngOnInit() { 
   

  }
}
