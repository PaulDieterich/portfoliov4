import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UeberMichComponent} from "./ueber-mich/ueber-mich.component";
import { ImpressumComponent } from './impressum/impressum.component';
import { ProjekteComponent } from './projekte/projekte.component';
import { SchuleComponent } from './schule/schule.component';
import { SelbststudiumComponent } from './selbststudium/selbststudium.component';
import { SkillsComponent } from './skills/skills.component';
import { DatenschutzComponent } from './datenschutz/datenschutz.component';
import { KontaktComponent } from './kontakt/kontakt.component';
import { HomeComponent } from './home/home.component';


const routes: Routes = [
  {path: "", component: HomeComponent  },
  {path: "uebermich", component:UeberMichComponent },
  {path: "impressum", component:ImpressumComponent },
  {path: "projekt", component:ProjekteComponent },
  {path: "schule", component:SchuleComponent },
  {path: "selbststudium", component:SelbststudiumComponent },
  {path: "skills", component:SkillsComponent },
  {path: "datenschutz", component: DatenschutzComponent},
  {path: "kontakt", component: KontaktComponent},
  {path: "home", component: HomeComponent  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
