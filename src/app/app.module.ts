import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatCheckboxModule } from '@angular/material';
import { NgModule } from '@angular/core';
import {HashLocationStrategy, LocationStrategy} from '@angular/common'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar'



import { UeberMichComponent } from './ueber-mich/ueber-mich.component';
import { SchuleComponent } from './schule/schule.component';
import { SkillsComponent } from './skills/skills.component';
import { SelbststudiumComponent } from './selbststudium/selbststudium.component';
import { ImpressumComponent } from './impressum/impressum.component';
import { ProjekteComponent } from './projekte/projekte.component';
import { DatenschutzComponent } from './datenschutz/datenschutz.component';
import { KontaktComponent } from './kontakt/kontakt.component';
import { HomeComponent } from './home/home.component';

import {HttpClientModule} from '@angular/common/http';
import { from } from 'rxjs';

@NgModule({
  declarations: [
    AppComponent,
   

    UeberMichComponent,
    SchuleComponent,
    SkillsComponent,
    SelbststudiumComponent,
    ImpressumComponent,
    ProjekteComponent,
    DatenschutzComponent,
    KontaktComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule, 
    MatCheckboxModule,
    HttpClientModule,


   
    
  ],
  providers: [
    {
      provide: LocationStrategy, useClass: HashLocationStrategy
    },
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
