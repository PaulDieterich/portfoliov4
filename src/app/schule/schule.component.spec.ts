import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchuleComponent } from './schule.component';

describe('SchuleComponent', () => {
  let component: SchuleComponent;
  let fixture: ComponentFixture<SchuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
